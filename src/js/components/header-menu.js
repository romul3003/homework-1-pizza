console.log('header-menu');
const navToggle = document.querySelector('.nav-toggle');
const headerNav = navToggle.closest('.header__nav-wrap').querySelector('.header__nav');

navToggle.addEventListener('click', (event) => {
	event.currentTarget.classList.toggle('js-change');
	headerNav.classList.toggle('is-open');
});


